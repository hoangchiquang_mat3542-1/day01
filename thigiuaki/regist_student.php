<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirm</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .container {
            width: 50%;
            margin: 0 auto;
            border: 2px solid #41719C;
        }

        span.col-sm-2.col-form-label {
            background-color: #70AD47;
            border: 2px solid #41719C;
            color: white;
            border-radius: 5px;
            margin-right: 25px;
        }

        .mb-3.row {
            margin: 15px 0px 15px 15px;
        }

        .submit {
            background-color: #70AD47;
            color: white;
            border: 2px solid #41719C;
            border-radius: 5px;
            font-size: 17px;
            padding: 10px 10px;
        }
    </style>
</head>

<body>
    <div class="container">
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST["name"];
            $gender = $_POST["sex"];
            if ($gender == 1) {
                $gender = "Nam";
            } else {
                $gender = "Nữ";
            }
            $date = $_POST["birthdate_day"] . "/" . $_POST["birthdate_month"] . "/" . $_POST["birthdate_year"];
            $address = $_POST["distric"] . " - " . $_POST["city"];
            $add_inf = $_POST["add_inf"];
            
            echo "<div class='mb-3 row'>";
            echo "<span class='col-sm-2 col-form-label'>Họ và tên</span>";
            echo "<div class='col-sm-5'>$name</div>";
            echo "</div>";

            echo "<div class='mb-3 row'>";
            echo "<span class='col-sm-2 col-form-label'>Giới tính</span>";
            echo "<div class='col-sm-5'>$gender</div>";
            echo "</div>";

            echo "<div class='mb-3 row'>";
            echo "<span class='col-sm-2 col-form-label'>Ngày sinh</span>";
            echo "<div class='col-sm-5'>$date</div>";
            echo "</div>";

            echo "<div class='mb-3 row'>";
            echo "<span class='col-sm-2 col-form-label'>Địa chỉ</span>";
            echo "<div class='col-sm-5'>$address</div>";
            echo "</div>";

            echo "<div class='mb-3 row'>";
            echo "<span class='col-sm-2 col-form-label'>Thông tin khác</span>";
            echo "<div class='col-sm-5'>$add_inf</div>";
            echo "</div>";
        } 
        ?>
    </div>
</body>

</html>