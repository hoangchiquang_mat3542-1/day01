<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .container {
            width: 50%;
            margin: 0 auto;
            border: 2px solid #41719C;
        }

        label.col-sm-2 {
            background-color: #70AD47;
            border: 2px solid #41719C;
            color: white;
            border-radius: 5px;
        }

        .form-check-inline {
            margin-top: 9px;
        }

        label {
            position: relative;
        }

        .col-sm-10 [type="text"],
        .col-sm-5 [type="date"],
        .col-sm-5 select,
        .col-sm-10 textarea {
            border: 2px solid #41719C;
        }

        [type="submit"] {
            background-color: #70AD47;
            color: white;
            border: 2px solid #41719C;
            border-radius: 5px;
            font-size: 17px;
            padding: 10px 10px;
        }
        .validate {
            color: red;
            font-weight: 500;
            margin-bottom: 20px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('[type="submit"]').on('click', () => {
                validateMessage = "";
                if ($('#name').val() == "") {
                    validateMessage += "Hãy nhập tên. <br>";
                }
                if ($('[name="sex"]:checked').val() == undefined) {
                    validateMessage += "Hãy chọn giới tính. <br>";
                }
                if ($('#birthdate_year').val() == "" || $('#birthdate_month').val() == "" || $('#birthdate_day').val() == "") {
                    validateMessage += "Hãy chọn ngày sinh. <br>";
                }
                if ($('#city').val() == "" || $('#distric').val() == "") {
                    validateMessage += "Hãy chọn địa chỉ. <br>";
                }
                $('.validate').html(validateMessage);
                if (validateMessage == "") {
                    $('form').submit();
                }
            })
        });
    </script>
</head>

<body>
    <div class="container p-5">
        <div class="text-center">
            <h4>Form đăng kí sinh viên</h4>
        </div>
        <div class="validate">
        </div>
        <form action="regist_student.php" method="post" enctype="multipart/form-data">
            <div class="mb-3 row">
                <label for="name" class="col-sm-2 col-form-label">Họ và tên</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="" class="col-sm-2 col-form-label">Giới tính</label>
                <div class="col-sm-10">
                    <?php
                    $sex_arr = array("Nam", "Nữ");
                    for ($i = 0; $i < count($sex_arr); $i++) {
                        $value = $sex_arr[$i];
                        $key = array_search($value, $sex_arr);
                        echo '<div class="form-check form-check-inline">';
                        echo '<input class="form-check-input" type="radio" name="sex" id="sex_checkbox_' . ($key+1) . '" value="' . ($key+1) . '">';
                        echo '<label class="form-check-label" for="sex_checkbox_' . ($key+1) . '">' . $value . '</label>';
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Ngày sinh</label>
                <div class="col-sm-3">
                    <label for="birthdate_year" class="col-form-label">Năm</label>
                    <select name="birthdate_year" class="form-select" id="birthdate_year">
                        <option value="" selected></option>
                        <?php
                        $current_year = date("Y");
                        for ($x = $current_year - 40; $x <= $current_year - 15; $x++) {
                            echo '<option value="' . $x . '">' . $x . '</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="col-sm-3">
                    <label for="birthdate_month" class="col-form-label ">Tháng</label>
                    <select name="birthdate_month" class="form-select" id="birthdate_month">
                        <option value="" selected></option>
                        <?php
                        for ($x = 1; $x <= 12; $x++) {
                            if ($x < 10) {
                                $x = "0" . $x;
                            }
                            echo '<option value="' . $x . '">' . $x . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="col-sm-3">
                    <label for="birthdate_day" class="col-form-label ">Ngày</label>
                    <select name="birthdate_day" class="form-select" id="birthdate_day">
                        <option value="" selected></option>
                        <?php
                        for ($x = 1; $x <= 31; $x++) {
                            if ($x < 10) {
                                $x = "0" . $x;
                            }
                            echo '<option value="' . $x . '">' . $x . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Địa chỉ</label>
                <div class="col-sm-3">
                    <label for="city" class="col-form-label">Thành phố</label>
                    <select name="city" class="form-select" id="city">
                        <option value="" selected></option>
                        <option value="Hà Nội">Hà Nội</option>
                        <option value="TP.Hồ Chí Minh">TP.Hồ Chí Minh</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <label for="distric" class="col-form-label">Quận</label>
                    <select name="distric" class="form-select" id="distric">
                        <option value="" selected></option>
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="add_inf" class="col-sm-2 col-form-label">Thông tin khác</label>
                <div class="col-sm-10">
                    <textarea name="add_inf" class="form-control" id="add_inf" rows="3"></textarea>
                </div>
            </div>
        </form>
        <div class="text-center">
            <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Đăng ký</button>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        const cityData = {
        "Hà Nội": ["", "Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
        "TP.Hồ Chí Minh": ["", "Quận 1", "Quận 2", "Quận 3", "Quận 4", "Quận 5"]
        };

        const $cityDropdown = $("#city");
        const $districtDropdown = $("#distric");

        $cityDropdown.on("change", function() {
            const selectedCity = $cityDropdown.val();
            const districts = cityData[selectedCity] || [];

            $districtDropdown.empty();
            $.each(districts, function(index, district) {
                $districtDropdown.append($("<option>", {
                    value: district,
                    text: district
                }));
            });
        });
    });
    </script>

</body>

</html>