<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <?php
    include '../day06/database.php';
    mysqli_query($conn, "USE ltweb");
    $facility_filter = "";
    $key_word = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $facility_filter = $_POST["facility"];
        if ($facility_filter == "MAT") {
            $facility_filter = "Khoa học máy tính";
        } else if ($facility_filter == "KDL") {
            $facility_filter = "Khoa học vật liệu";
        }
        $key_word = $_POST["key_word"];
        if ($facility_filter == "all") {
            $sql = "SELECT * FROM `students` WHERE FULL_NAME LIKE '%$key_word%' OR COUNTRY LIKE '%$key_word%' OR GENDER LIKE '%$key_word%' OR BIRTH_DATE LIKE '%$key_word%'";
        } else {
            $sql = "SELECT * FROM `students` WHERE FACILITY='$facility_filter' AND (FULL_NAME LIKE '%$key_word%' OR COUNTRY LIKE '%$key_word%' OR GENDER LIKE '%$key_word%' OR BIRTH_DATE LIKE '%$key_word%')";
        }
    } else {
        $sql = "SELECT * FROM `students`";
    }
    $result = mysqli_query($conn, $sql); 
    $count = $result->num_rows;
    ?>

    <div class="container filter">
        <form action="" method="POST">
            <div class="mb-3 row">
                <label for="select_facility" class="offset-sm-2 col-sm-2 col-form-label ">Khoa</label>
                <div class="col-sm-5">
                    <select name="facility" class="form-select" id="select_facility">
                        <?php
                        if ($facility_filter == "Khoa học máy tính") {
                            echo "<option value='all'>Tất cả</option>";
                            echo "<option value='MAT' selected>Khoa học máy tính</option>";
                            echo "<option value='KDL'>Khoa học vật liệu</option>";
                        } else if ($facility_filter == "Khoa học vật liệu") {
                            echo "<option value='all'>Tất cả</option>";
                            echo "<option value='MAT'>Khoa học máy tính</option>";
                            echo "<option value='KDL' selected>Khoa học vật liệu</option>";
                        } else {
                            echo "<option value='all' selected>Tất cả</option>";
                            echo "<option value='MAT'>Khoa học máy tính</option>";
                            echo "<option value='KDL'>Khoa học vật liệu</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="key_word" class="offset-sm-2 col-sm-2 col-form-label ">Từ khóa</label>
                <div class="col-sm-5">
                    <?php
                    echo "<input type='text' class='form-control' id='key_word' name='key_word' value='$key_word'>"; 
                    ?>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Tìm kiếm</button>
            </div>
        </form>
    </div>

    <div class="container list-student">
        <div class="mb-3 row">
            <div class="col-sm-3">
                Số sinh viên tìm thấy:
                <?php 
                echo "$count";
                ?>
            </div>
            <div class="offset-sm-8 col-sm-1">
                <a href="register.php" class="btn btn-success">Thêm</a>
            </div>
        </div>
        <div class="mb-3 row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tên sinh viên</th>
                        <th scope="col">Khoa</th>
                        <th scope="col" class="text-end">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if ($count > 0) {
                        $no = 1;
                        while ($row = $result->fetch_assoc()) {
                            echo "<tr>";
                            echo "<th scope='row'>" . $no . "</th>";
                            echo "<td>" . $row["FULL_NAME"] . "</td>";
                            echo "<td>" . $row["FACILITY"] . "</td>";
                            echo "<td class='text-end'>";
                            echo "<a class='btn btn-primary ms-3'>Xóa</a>";
                            echo "<a class='btn btn-danger ms-3'>Sửa</a>";
                            echo "</td>";
                            echo "</tr>";
                            $no++;
                        }
                    } else {
                        echo "<tr>";
                        echo "<td colspan='4' class='text-center'>Không có sinh viên nào</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- <script type="text/javascript">
        $(document).ready(function() {
            $("#select_facility").change(function() {
                $.ajax({
                    url: "https://localhost:7033/api/Books",
                    method: "GET",
                    success: function(data) {
                        console.log(data)
                    }
                });
            });
        });
    </script> -->
</body>

</html>