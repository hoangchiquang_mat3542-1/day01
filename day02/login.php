<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng nhập</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>

    <style>
        .bg-info, .col-6, .btn-info
        {
            border: 2px solid #2d73e3;
            color: #f1f1f1;
        }
    </style>
</head>

<body>
    <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $day_month_year = date('d/m/Y');
        $hour_minute = date('H') . ":" . date('i');
        $weekday = date('w');
        if ($weekday == 0) {
            $weekday = "Chủ nhật";
        } else {
            $weekday = "thứ " . ($weekday + 1);
        }
    ?>

    <div class="container-fuild">
        <div class="row">
            <div class="offset-md-3 col-md-6 text-center">
                <div class="bg-light pt-2 pb-2">Bây giờ là: <?php echo $hour_minute . ", " . $weekday . " ngày " . $day_month_year;  ?></div>
            </div>
        </div>
        <div class="row">
            <form action="login_post.php" method="post" class="col-md-6 offset-md-3">
                <div class="mt-3">
                    <label for="username" class="col-5 bg-info p-1">Tên đăng nhập</label>
                    <input type="text" id="username" name="username" class="col-6 p-1 float-end" required>
                </div>

                <div class="mt-3">
                    <label for="password" class="col-5 bg-info p-1">Mật khẩu</label>
                    <input type="password" id="password" name="password" class="col-6 p-1 float-end" required>
                </div>

                <div class="mt-3 text-center">
                    <input type="submit" class="btn btn-info" value="Đăng nhập">
                </div>
            </form>
        </div>
    </div>
</body>

</html>