<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="mb-3 row">
            <label for="select_facility" class="offset-sm-2 col-sm-2 col-form-label ">Khoa</label>
            <div class="col-sm-5">
                <select name="facility" class="form-select" id="select_facility">
                    <option value='all' selected>Tất cả</option>
                    <option value='MAT'>Khoa học máy tính</option>
                    <option value='KDL'>Khoa học vật liệu</option>
                </select>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="key_word" class="offset-sm-2 col-sm-2 col-form-label ">Từ khóa</label>
            <div class="col-sm-5">
                <input type='text' class='form-control' id='key_word' name='key_word'>
            </div>
        </div>
        <div class="text-center">
            <button id="reset" class="btn btn-secondary mb-3 pe-5 ps-5">Reset</button>
        </div>
    </div>

    <div class="container">
        <div class="mb-3 row">
            <div class="col-sm-4">
                Số sinh viên tìm thấy: <span id="total"></span>
            </div>
            <div class="offset-sm-7 col-sm-1 p-0">
                <a href="../../day05/register.php" class="btn btn-success">Thêm</a>
            </div>
        </div>
        <div class="mb-3 row">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col" style="width: 15%">No</th>
                        <th scope="col" style="width: 35%">Tên sinh viên</th>
                        <th scope="col" style="width: 35%">Khoa</th>
                        <th scope="col" colspan="2" class="text-center" style="width: 15%">Action</th>
                    </tr>
                </thead>
                <tbody class="list-student table-group-divider">
                </tbody>
            </table>
        </div>
    </div>

    <!-- Button trigger modal -->
    <button type="button" id="delete_popup" class="btn btn-primary d-none" data-bs-toggle="modal" data-bs-target="#delete"></button>

    <!-- Modal -->
    <div class="modal fade" id="delete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Xóa sinh viên</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Bạn muốn xóa sinh viên này?
                    <input id="delete_id" type="text" class="d-none"></input>
                </div>
                <div class="modal-footer">
                    <button type="button" id="delete_item" class="btn btn-secondary" data-bs-dismiss="modal">Xóa</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>

    <script src="../site.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            searchItems();
            $("#reset").on("click", () => {
                $("#key_word").val("");
                $("#select_facility").val("all");
                searchItems();
            });
            $("#key_word").on("keyup", () => {
                searchItems();
            });
            $("#select_facility").on("change", () => {
                searchItems();
            });
            $("#delete_item").on("click", () => {
                deleteItem($("#delete_id").val());
                searchItems();
            });
        });
    </script>
</body>

</html>