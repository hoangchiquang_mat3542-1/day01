<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .container {
            width: 50%;
            margin: 0 auto;
            border: 2px solid #41719C;
        }

        label.col-sm-2 {
            background-color: #70AD47;
            border: 2px solid #41719C;
            color: white;
            border-radius: 5px;
        }

        .form-check-inline {
            margin-top: 9px;
        }

        label {
            position: relative;
        }

        .col-sm-10 [type="text"],
        .col-sm-5 [type="date"],
        .col-sm-5 select,
        .col-sm-10 textarea {
            border: 2px solid #41719C;
        }

        [type="submit"] {
            background-color: #70AD47;
            color: white;
            border: 2px solid #41719C;
            border-radius: 5px;
            font-size: 17px;
            padding: 10px 10px;
        }

        .require {
            color: red;
            font-size: 20px;
            position: absolute;
            bottom: 10px;
            right: 5px;
        }

        .validate {
            color: red;
            font-weight: 500;
            margin-bottom: 20px;
        }
    </style>
</head>

<body>
    <div class="container p-5">
        <div class="validate">
        </div>
        <form id="updateForm" action="" method="post" enctype="multipart/form-data">
            <?php
            $id = $_GET['id'];
            echo "<input type='text' name='id' id='id' class='d-none' value='$id'>";
            ?>
            <div class="mb-3 row">
                <label for="full_name" class="col-sm-2 col-form-label">Họ và tên<span class="require">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="full_name" name="full_name">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="" class="col-sm-2 col-form-label">Giới tính<span class="require">*</span></label>
                <div class="col-sm-10">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sex" id="Male" value="Male">
                        <label class="form-check-label" for="Male">Nam</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sex" id="Female" value="Female">
                        <label class="form-check-label" for="Female">Nữ</label>
                    </div>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="facility" class="col-sm-2 col-form-label ">Phân khoa<span class="require">*</span></label>
                <div class="col-sm-5">
                    <select name="facility" class="form-select" id="facility">
                        <option value="">--Chọn phân khoa--</option>
                        <option value="MAT">Khoa học máy tính</option>
                        <option value="KDL">Khoa học vật liệu</option>
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="birthday" class="col-sm-2 col-form-label ">Ngày sinh<span class="require">*</span></label>
                <div class="col-sm-5">
                    <input type="date" class="form-control" id="birthday" name="birthday">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="address" class="col-sm-2 col-form-label" style="height: 30%;">Địa chỉ</label>
                <div class="col-sm-10">
                    <textarea name="address" class="form-control" id="address" rows="3"></textarea>
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label" for="another_img" style="height: 20%;">Hình ảnh</label>
                <div class="col-sm-10">
                    <div class='col-sm-5'>
                        <img src='' id="img_upload" alt='Uploaded Image' class='img-fluid'>
                        <input type='text' name='img_path' id='img_path' class='d-none'>
                    </div>
                    <input id="another_img" type="file" name="fileToUpload" class="form-control">
                </div>
            </div>
        </form>
        <div class="text-center">
            <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Xác nhận</button>
        </div>
    </div>
    <script src="../site.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            displayEditItem($("#id").val());
            $('[type="submit"]').on('click', () => {
                validateMessage = "";
                let name = $('#full_name').val();
                let gender = $('[name="sex"]:checked').val();
                let facility = $('#facility').val();
                let date = $('#birthday').val();
                let address = $('#address').val();
                if (name == "") {
                    validateMessage += "Hãy nhập tên. <br>";
                }
                if (gender == undefined) {
                    validateMessage += "Hãy chọn giới tính. <br>";
                }
                if (facility == "") {
                    validateMessage += "Hãy chọn phân khoa. <br>";
                }
                if (date == "") {
                    validateMessage += "Hãy chọn ngày sinh. <br>";
                } else if ((date.slice(0, date.indexOf('-'))) > new Date().getFullYear()) {
                    validateMessage += "Hãy nhập ngày sinh đúng định dạng. <br>";
                }
                $('.validate').html(validateMessage);
                if (validateMessage == "") {
                    updateItem();
                }
            });
        });
    </script>
</body>

</html>