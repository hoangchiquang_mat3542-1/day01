function searchItems() {
    var k = $("#key_word").val();
    var f = $("#select_facility").val();
    $.ajax({
        url: "../controllers/search.php",
        type: "POST",
        data: {
            key_word: k,
            facility: f
        },
        success: function(response) {
            console.log(response);
            displayItems(response);
        }
    })
}

function displayEditItem(id) {
    $.ajax({
        url: "../controllers/get.php",
        type: "POST",
        data: {
            id: id
        },
        success: function(response) {
            console.log(response);
            showEditItem(response);
        }
    })
}

function showEditItem(data) {
    data = JSON.parse(data);
    console.log(data);
    $("#id").val(data.ID);
    $("#full_name").val(data.FULL_NAME);
    var gender = data.GENDER;
    $("#"+gender).prop("checked", true);
    $("#facility option[value='"+data.FACILITY+"']").prop("selected", true);
    $("#birthday").val(data.BIRTH_DATE);
    $("#address").val(data.COUNTRY);
    $("#img_upload").attr("src", '../uploads/'+data.IMG_PATH);
    $("#img_path").val(data.IMG_PATH);
}

function updateItem() {
    var formData = new FormData($("#updateForm")[0]);

    var fileInput = $('#another_img')[0];
    if (fileInput.files.length > 0) {
        formData.append('fileToUpload', fileInput.files[0]);
    }
    $.ajax({
        url: "../controllers/update.php",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
            alert(response);
            // Display the alert, wait for a moment, and then redirect
            setTimeout(function() {
                window.location.href = 'students.php';
            }, 1000); // Redirect after 1000 milliseconds (1 second)
        }
    });
}

function deleteItem(id) {
    $.ajax({
        url: "../controllers/delete.php",
        type: "POST",
        data: {
            id: id
        },
        success: function(response) {
            console.log(response);
            alert(response);
            searchItems();
        }
    })
}

function displayItems(data) {
    var html = "";
    data = JSON.parse(data);
    $("#total").html(data.length);
    for (var i = 0; i < data.length; i++) {
        html += "<tr>";
        html += "<td>" + (i+1) + "</td>";
        html += "<td class='d-none'>" + data[i].ID + "</td>";
        html += "<td>" + data[i].FULL_NAME + "</td>";
        var facility = data[i].FACILITY;
        if (facility == "MAT") {
            facility = "Khoa học máy tính";
        } else {
            facility = "Khoa học vật liệu";
        }
        html += "<td>" + facility + "</td>";
        html += `<td class='text-end'><button class='btn btn-danger delete_btn'>Xóa</button></td>`;
        html += `<td class='text-end'><a href="update_students.php?id=${data[i].ID}" class='btn btn-info display_edit'>Sửa</button></td>`;
        html += "</tr>";
    }
    $(".list-student").html(html);

    $(document).on('click', '.delete_btn', function() {
        var id = $(this).closest("tr").find(".d-none").text();
        console.log("ID to be deleted: " + id);
        $("#delete_popup").click();
        $("#delete_id").val(id);
    });
    
}
