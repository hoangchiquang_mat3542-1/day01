function searchItems() {
    var k = $("#key_word").val();
    var f = $("#select_facility").val();
    $.ajax({
        url: "search.php",
        type: "POST",
        data: {
            key_word: k,
            facility: f
        },
        success: function(response) {
            console.log(response);
            displayItems(response);
        }
    })
}

function displayItems(data) {
    var html = "";
    data = JSON.parse(data);
    $("#total").html(data.length);
    for (var i = 0; i < data.length; i++) {
        html += "<tr>";
        html += "<td>" + (i+1) + "</td>";
        html += "<td>" + data[i].FULL_NAME + "</td>";
        html += "<td>" + data[i].FACILITY + "</td>";
        html += `<td class='text-end'><button class='btn btn-danger'>Xóa</button></td>`;
        html += `<td class='text-end'><button class='btn btn-info'>Sửa</button></td>`;
        html += "</tr>";
    }
    $(".list-student").html(html);
}