<?php
    include '../day06/database.php';
    mysqli_query($conn, "USE ltweb");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $facility = $_POST["facility"];
        $key_word = $_POST["key_word"];
        if ($facility == "MAT") {
            $facility = "Khoa học máy tính";
        } else if ($facility == "KDL") {
            $facility = "Khoa học vật liệu";
        }

        $sql = "SELECT * FROM `students` WHERE (FULL_NAME LIKE '%$key_word%')";
        if ($facility != "all") {
            $sql = $sql . " AND FACILITY='$facility'";
        } 
        $result = mysqli_query($conn, $sql);
        if ($result) {
            $list_student = array();
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($list_student, $row);
            }
            echo json_encode($list_student, JSON_UNESCAPED_UNICODE);
            mysqli_free_result($result);
        } else {
            echo "Truy vấn thất bại: " . mysqli_error($conn);
        }
    }
    mysqli_close($conn);
