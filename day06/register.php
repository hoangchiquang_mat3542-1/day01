<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .container {
            width: 50%;
            margin: 0 auto;
            border: 2px solid #41719C;
        }

        label.col-sm-2 {
            background-color: #70AD47;
            border: 2px solid #41719C;
            color: white;
            border-radius: 5px;
        }

        .form-check-inline {
            margin-top: 9px;
        }

        label {
            position: relative;
        }

        .col-sm-10 [type="text"],
        .col-sm-5 [type="date"],
        .col-sm-5 select,
        .col-sm-10 textarea {
            border: 2px solid #41719C;
        }

        [type="submit"] {
            background-color: #70AD47;
            color: white;
            border: 2px solid #41719C;
            border-radius: 5px;
            font-size: 17px;
            padding: 10px 10px;
        }

        .require {
            color: red;
            font-size: 20px;
            position: absolute;
            bottom: 10px;
            right: 5px;
        }

        .validate {
            color: red;
            font-weight: 500;
            margin-bottom: 20px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('[type="submit"]').on('click', () => {
                validateMessage = "";
                let full_name = $('#full_name').val();
                let gender = $('[name="sex"]:checked').val();
                let facility = $('#select_facility').val();
                let date = $('#date').val();
                if (full_name == "") {
                    validateMessage += "Hãy nhập tên. <br>";
                }
                if (gender == undefined) {
                    validateMessage += "Hãy chọn giới tính. <br>";
                }
                if (facility == "") {
                    validateMessage += "Hãy chọn phân khoa. <br>";
                }
                if (date == "") {
                    validateMessage += "Hãy chọn ngày sinh. <br>";
                } else if ((date.slice(0, date.indexOf('-'))) > new Date().getFullYear()) {
                    validateMessage += "Hãy nhập ngày sinh đúng định dạng. <br>";
                }
                $('.validate').html(validateMessage);
                if (validateMessage == "") {
                    $('form').submit();
                }
            })
        });
    </script>
</head>

<body>
    <?php
    $sex_arr = array("Nam", "Nữ");
    $facility_arr = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
    ?>
    <div class="container p-5">
        <div class="validate">
        </div>
        <form action="confirm.php" method="post" enctype="multipart/form-data">
            <div class="mb-3 row">
                <label for="full_name" class="col-sm-2 col-form-label">Họ và tên<span class="require">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="full_name" name="full_name">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="" class="col-sm-2 col-form-label">Giới tính<span class="require">*</span></label>
                <div class="col-sm-10">
                    <?php
                    for ($i = 0; $i < count($sex_arr); $i++) {
                        $value = $sex_arr[$i];
                        $key = array_search($value, $sex_arr);
                        echo '<div class="form-check form-check-inline">';
                        echo '<input class="form-check-input" type="radio" name="sex" id="sex_checkbox_' . $key . '" value="' . $key . '">';
                        echo '<label class="form-check-label" for="sex_checkbox_' . $key . '">' . $value . '</label>';
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="select_facility" class="col-sm-2 col-form-label ">Phân khoa<span class="require">*</span></label>
                <div class="col-sm-5">
                    <select name="facility" class="form-select" id="select_facility">
                        <option value="" selected>--Chọn phân khoa--</option>
                        <?php
                        foreach ($facility_arr as $key => $value) {
                            echo '<option value="' . $key . '">' . $value . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="date" class="col-sm-2 col-form-label ">Ngày sinh<span class="require">*</span></label>
                <div class="col-sm-5">
                    <input type="date" class="form-control" id="date" name="date">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="address" class="col-sm-2 col-form-label" style="height: 30%;">Địa chỉ</label>
                <div class="col-sm-10">
                    <textarea name="address" class="form-control" id="address" rows="3"></textarea>
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label" for="fileToUpload">Hình ảnh</label>
                <div class="col-sm-10">
                    <input type="file" name="fileToUpload" class="form-control" id="fileToUpload">
                </div>
            </div>
        </form>
        <div class="text-center">
            <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Đăng ký</button>
        </div>
    </div>
</body>

</html>