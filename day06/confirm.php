<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirm</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .container {
            width: 50%;
            margin: 0 auto;
            border: 2px solid #41719C;
        }

        span.col-sm-2.col-form-label {
            background-color: #70AD47;
            border: 2px solid #41719C;
            color: white;
            border-radius: 5px;
            margin-right: 25px;
        }

        .mb-3.row {
            margin: 15px 0px 15px 15px;
        }

        .submit {
            background-color: #70AD47;
            color: white;
            border: 2px solid #41719C;
            border-radius: 5px;
            font-size: 17px;
            padding: 10px 10px;
        }
    </style>
</head>

<body>
    <?php
    if (isset($_FILES["fileToUpload"])) {
        $folderPath = __DIR__ . '/uploads';
        if (!file_exists($folderPath)) {
            mkdir($folderPath, 0777, true);
        }
        $imageFile = $_FILES["fileToUpload"]["name"];
        $imageTemp = $_FILES["fileToUpload"]["tmp_name"];
        $targetFile = __DIR__ . '/uploads/' . $imageFile;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        $check = getimagesize($imageTemp);
        if ($check !== false) {
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
        }

        // Check if the file already exists in the same path
        if (file_exists($targetFile)) {
            $uploadOk = 0;
        }

        // Check for uploaded file formats and allow only jpg, png, jpeg and gif
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 0;
        }

        if ($uploadOk == 1) {
            if (isset($_FILES["fileToUpload"]) && $_FILES["fileToUpload"]["error"] == 0) {
                move_uploaded_file($imageTemp, $targetFile);
            }
        }
    } else {
        $imageFile = $_POST["fileToUpload"];
    }
    ?>
    <div class="container">
        <?php
        include 'database.php'; 
        mysqli_query($conn, "USE ltweb");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $full_name = $_POST["full_name"];
            $gender = $_POST["sex"];
            if ($gender == 0) {
                $gender = "Nam";
            } else {
                $gender = "Nữ";
            }
            $facility = $_POST["facility"];
            if ($facility == "MAT") {
                $facility = "Khoa học máy tính";
            } else {
                $facility = "Khoa học vật liệu";
            }
            $date = $_POST["date"];
            $address = $_POST["address"];
        }
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["confirmButton"])) {
            $sql = "INSERT INTO `students` (`FULL_NAME`, `GENDER`, `FACILITY`, `BIRTH_DATE`, `COUNTRY`, `IMG_PATH`) 
            VALUES ('$full_name', '$gender', '$facility', '$date', '$address', '$imageFile')";

            if (mysqli_query($conn, $sql)) {
                echo "Dữ liệu đã được thêm vào cơ sở dữ liệu.";
            } else {
                echo "Lỗi: " . mysqli_error($conn);
            }
        }
        ?>
        <form method="POST" action="" enctype="multipart/form-data">
            <div class='mb-3 row'>
                <span class='col-sm-2 col-form-label'>Họ và tên</span>
                <div class='col-sm-5'>
                    <?php
                    echo $full_name;
                    echo "<input type='hidden' name='full_name' value='$full_name'>" 
                    ?>
                </div>
            </div>

            <div class='mb-3 row'>
                <span class='col-sm-2 col-form-label'>Giới tính</span>
                <div class='col-sm-5'>
                    <?php
                    echo $gender;
                    echo "<input type='hidden' name='sex' value='$gender'>" 
                    ?>
                </div>
            </div>

            <div class='mb-3 row'>
                <span class='col-sm-2 col-form-label'>Phân khoa</span>
                <div class='col-sm-5'>
                    <?php
                    echo $facility;
                    echo "<input type='hidden' name='facility' value='$facility'>" 
                    ?>
                </div>
            </div>

            <div class='mb-3 row'>
                <span class='col-sm-2 col-form-label'>Ngày sinh</span>
                <div class='col-sm-5'>
                    <?php
                    echo date("d/m/Y", strtotime($date));
                    echo "<input type='hidden' name='date' value='$date'>" 
                    ?>
                </div>
            </div>

            <div class='mb-3 row'>
                <span class='col-sm-2 col-form-label'>Địa chỉ</span>
                <div class='col-sm-5'>
                    <?php
                    echo $address;
                    echo "<input type='hidden' name='address' value='$address'>"
                    ?>
                </div>
            </div>

            <div class='mb-3 row'>
                <span class='col-sm-2 col-form-label' style='height:30%'>Hình ảnh</span>
                <div class='col-sm-5'>
                    <?php
                    echo "<img src='uploads/$imageFile' alt='Uploaded Image' class='img-fluid'>";
                    echo "<input type='hidden' name='fileToUpload' value='$imageFile'>" 
                    ?>
                </div>
                <div class='text-center'>
                    <button type='submit' name='confirmButton' class='submit btn btn-success mb-3 pe-5 ps-5 mt-3'>Xác nhận</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html>