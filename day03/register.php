<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .container {
            width: 50%;
            margin: 0 auto;
            border: 2px solid #41719C;
        }
        label.col-sm-3 {
            background-color: #5b9bd5;
            border: 2px solid #41719C;
            color: white;
        }
        .form-check-inline {
            margin-top: 9px;
        }
    </style>
</head>
<body>
    <?php
        $sex_arr = array("Nam","Nữ");
        $facility_arr = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
    ?>
    <div class="container p-5">
        <form action="">
            <div class="mb-3 row">
                <label for="name" class="col-sm-3 col-form-label">Họ và tên</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>

            <div class="mb-3 row">
                <label for="" class="col-sm-3  col-form-label">Giới tính</label>
                <div class="col-sm-8">
                    <?php
                        for ($i = 0; $i < count($sex_arr); $i++) {
                            $value = $sex_arr[$i];
                            $key = array_search($value, $sex_arr);
                            echo '<div class="form-check form-check-inline">';
                            echo '<input class="form-check-input" type="radio" name="sex" id="sex_checkbox_' . $key . '" value="' . $key . '">';
                            echo '<label class="form-check-label" for="sex_checkbox_' . $key . '">' . $value . '</label>';
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>

            <div class="mb-3 row">
                <label for="select_facility" class="col-sm-3">Phân khoa</label>
                <div class="col-sm-5">
                    <select name="facility" class="form-select" id="select_facility">
                        <option value="" selected>--Chọn phân khoa--</option>
                        <?php
                            foreach($facility_arr as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-success mb-3 pe-5 ps-5">Đăng ký</button>
            </div>
        </form>
    </div>

</body>
</html>